# Sliding Login Signup Form

This is a Login form and a Signup form. Built with HTML5, CSS3 and JavaScript. 

Just download the respository and unzip the file. <br> 
Open index.html file and here you go. <br>
Enjoy the responsive login and signup page with sliding effect. <br> 

![form](/uploads/6272646743c1de85942117a2b90201cc/form.gif)

<h2> Feel free to play with Login and Signup. If you have a crazy idea, then pull request ☺ </h2> 
